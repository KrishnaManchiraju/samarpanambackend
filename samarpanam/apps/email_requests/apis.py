import numpy
import logging, requests, json

from rest_framework.decorators import api_view, permission_classes
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework import permissions, status

from django.contrib.auth.models import User
from django.utils.timezone import now
from django.core.paginator import Paginator, EmptyPage
from django.conf import settings

from .models import Audience, EmailRequest, EmailType, Review, Status, EmailProvider
from .serializers import (
    AudienceSerializer,
    UserSerializer,
    EmailRequestGetSerializer,
    EmailRequestGetPartialSerializer,
    EmailRequestPostSerializer,
    EmailRequestUpdateSerializer,
    EmailRequestCompletionSerializer,
    EmailTypeSerializer,
    StatusSerializer,
    EmailProviderChoicesSerializer,
    MailchimpStatsSerializer
)

from samarpanam.emails import email_service

logger = logging.getLogger("samarpanam")


@api_view(["GET"])
def get_all_users(request: Request) -> Response:
    users = User.objects.all()
    serializer = UserSerializer(users, many=True)

    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_all_audience(request: Request) -> Response:
    audis = Audience.objects.all()
    serializer = AudienceSerializer(audis, many=True)

    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_all_requests(request: Request) -> Response:
    logger.debug("Requesting for all requests")
    logger.debug(request)

    page = None
    try:
        page = int(request.GET.get("page", 1))
    except ValueError:
        return Response(
            {"message": "Incorrect Page Number Requested"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    try:
        page_size = int(request.GET.get("page_size", 10))
    except ValueError:
        page_size = 10

    req_list = EmailRequest.objects.order_by("-expected_delivery")
    paginator = Paginator(req_list, page_size)

    try:
        reqs = paginator.page(page)
    except EmptyPage:
        reqs = paginator.page(paginator.num_pages)

    previous = None if not reqs.has_previous() else reqs.previous_page_number()
    next = None if not reqs.has_next() else reqs.next_page_number()

    serializer = EmailRequestGetPartialSerializer(reqs, many=True)
    response = {"previous": previous, "next": next, "results": serializer.data}

    return Response(response, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_all_email_types(request: Request) -> Response:
    types = EmailType.objects.all()
    serializer = EmailTypeSerializer(types, many=True)

    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_all_status(request: Request) -> Response:
    statuses = Status.objects.all()
    serializer = StatusSerializer(statuses, many=True)

    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(["GET"])
def get_detailed_request(request: Request) -> Response:
    try:
        req = EmailRequest.objects.get(id=request.query_params["id"])
        serializer = EmailRequestGetSerializer(req)

        return Response(serializer.data, status=status.HTTP_200_OK)
    except EmailRequest.DoesNotExist:
        logger.error(
            "get_detailed_request: id {} does not exist".format(
                request.query_params["id"]
            )
        )
        return Response(
            {"message": "Record not found"}, status=status.HTTP_404_NOT_FOUND
        )


@api_view(["POST"])
def create_new_request(request: Request) -> Response:
    """
    Inputs:
        email_subject
        audience
        expected_delivery
        reviewers
        details
        created_by
    """

    serializer = EmailRequestPostSerializer(data=request.data)

    if serializer.is_valid():
        new_req = serializer.save()
        # Audience
        audi = Audience.objects.filter(id__in=request.data.getlist("audience"))
        new_req.audience.add(*audi)
        # Reviewers
        reviewers = User.objects.filter(id__in=request.data.getlist("reviewers"))
        for reviewer in reviewers:
            review = Review.objects.create(request=new_req, reviewer=reviewer)

        # Status
        new_req.status = Status.objects.get(status="Pending")

        new_req.save()

        # send email
        email_service.send_create_notification(new_req)

    else:
        logger.error("create_new_request: " + str(serializer.errors))
        return Response(
            {"message": "Invalid Request"}, status=status.HTTP_400_BAD_REQUEST
        )

    return Response({"message": "Request Created"}, status=status.HTTP_201_CREATED)


@api_view(["POST"])
def update_request(request: Request) -> Response:
    """
    Inputs:
        email_subject
        audience
        expected_delivery
        reviewers
        details
        modified_by
    """
    up_req = EmailRequest.objects.get(id=request.data["id"])
    serializer = EmailRequestUpdateSerializer(up_req, data=request.data)

    if serializer.is_valid():
        req = serializer.save()
        # Audience
        req.audience.clear()
        audi = Audience.objects.filter(id__in=request.data.getlist("audience"))
        req.audience.add(*audi)

        req.modified_at = now()
        req.save()

        # remove reviewers not in the update request
        currentreviews = Review.objects.filter(request__id=request.data["id"])
        currentreviews.exclude(reviewer__id__in=request.data.getlist("reviewers")).delete()

        # workout new reviewers to add 
        newreviewer_ids = numpy.setdiff1d( 
                                numpy.array(request.data.getlist("reviewers")).astype('int'), # updation list
                                list(currentreviews.values_list("reviewer_id", flat=True)) # current list
                            )
        addreviewers = User.objects.filter(id__in=newreviewer_ids)
        for reviewer in addreviewers:
            review = Review.objects.create(request=req, reviewer=reviewer)

    else:
        logger.error("update_request: " + str(serializer.errors))
        return Response(
            {"message": "Invalid Request"}, status=status.HTTP_400_BAD_REQUEST
        )

    return Response({"message": "Request Updated"}, status=status.HTTP_201_CREATED)


@api_view(["PUT"])
def approve_request(request: Request) -> Response:
    """
    Inputs:
        review_id : Note: We just update the review record. Not the request
    """
    try:
        review = Review.objects.get(id=request.data.get("review_id"))
        review.approved = True
        review.save()

        pending_count = Review.objects.filter(
            request=review.request, approved=False
        ).count()
        if pending_count == 0:
            completed_status = Status.objects.get(
                step=5
            )  # 5 is "all approved". ToDo: Find a betterway to avoid these magic numbers
            review.request.status = completed_status
            review.request.save()
            email_service.send_status_update(review.request)

        return Response({"message": "Approved"}, status=status.HTTP_200_OK)
    except EmailRequest.DoesNotExist:
        logger.error(
            f"approve_request: id {request.data.get('review_id')} does not exist"
        )
        return Response(
            {"message": "Review record not found"}, status=status.HTTP_404_NOT_FOUND
        )


@api_view(["PUT"])
def update_request_status(request: Request) -> Response:
    """
    Inputs:
        id : id of the request for which the status needs to be updated
        step: (int) current status step of the request
    """
    try:
        req = EmailRequest.objects.get(id=request.data.get("id"))
        req_status = Status.objects.get(step=request.data.get("step"))

        req.status = req_status
        req.save()

        email_service.send_status_update(req)

        serializer = StatusSerializer(req_status)

        return Response(serializer.data, status=status.HTTP_200_OK)
    except EmailRequest.DoesNotExist:
        logger.error(
            f"update_request_status: id {request.data.get('id')} does not exist"
        )
        return Response(
            {"message": "Email Request record not found"},
            status=status.HTTP_404_NOT_FOUND,
        )
    except Status.DoesNotExist:
        logger.error(
            f"update_request_status: step {request.data.get('step')} does not exist"
        )
        return Response(
            {"message": "Status step not found"}, status=status.HTTP_404_NOT_FOUND
        )

@api_view(["PUT"])
def complete_request(request: Request) -> Response:
    """
    Inputs:
        id : id of the request to mark as complete
        email_provider
        email_provider_request_ref
    """
    COMPLETION_STEP = 6

    up_req = EmailRequest.objects.get(id=request.data["id"])
    serializer = EmailRequestCompletionSerializer(up_req, data=request.data)

    if serializer.is_valid():
        req = serializer.save()
        req.modified_at = now()
        req_status = Status.objects.get(step=COMPLETION_STEP)

        req.status = req_status
        req.save()

        email_service.send_status_update(req)
        serializer = EmailRequestGetSerializer(req)
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        logger.error("complete_request_provider: " + str(serializer.errors))
        return Response(
            {"message": "Invalid Request"}, status=status.HTTP_400_BAD_REQUEST
        )

@api_view(["GET"])
def get_email_providers(request: Request) -> Response:
    try:    
        serializer = EmailProviderChoicesSerializer(EmailProvider.choices, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    except Exception as e:
        logger.error("get_email_providers: " + str(e))
        return Response(
            {"message": "Error getting email providers"}, status=status.HTTP_404_NOT_FOUND
        )

@api_view(["GET"])
def get_request_stats(request: Request) -> Response:
    """
    Inputs:
        id : id of the request
    """       
    try:
        req = EmailRequest.objects.get(id=request.query_params["id"]) 
        reqCompSerializer = EmailRequestCompletionSerializer(req)
        reqComp = reqCompSerializer.data
        if (
            (reqComp['email_provider'] and reqComp['email_provider'] == EmailProvider.Mailchimp.value) and
            (reqComp['email_provider_request_ref'] and not reqComp['email_provider_request_ref'].isspace())                 
        ):
            stats = requests.get(
                "%s/%s/%s" % (settings.EMAIL_PROVIDERS[reqComp['email_provider']]['BASE_URL'], "reports", reqComp['email_provider_request_ref']), 
                headers={'Authorization':'Bearer %s' % settings.EMAIL_PROVIDERS[reqComp['email_provider']]['API_KEY']}
            )
            serializer = MailchimpStatsSerializer(stats.json())
            return Response(serializer.data, status=status.HTTP_200_OK)

        return Response(None, status=status.HTTP_200_OK)        
    except Exception as e:
        logger.error("get_request_stats: " + str(e))
        return Response(
            {"message": "Error getting request stats"}, status=status.HTTP_404_NOT_FOUND
        )        
