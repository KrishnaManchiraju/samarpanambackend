from django.urls import path
from . import apis

urlpatterns = [
    path("create/", apis.create_new_request, name="create request"),
    path("update/", apis.update_request, name="update request"),
    path("list/", apis.get_all_requests, name="request list"),
    path("detail/", apis.get_detailed_request, name="detailed request"),
    path("audience/", apis.get_all_audience, name="audience"),
    path("reviewers/", apis.get_all_users, name="reviewrs"),
    path("types/", apis.get_all_email_types, name="email types"),
    path("status/", apis.get_all_status, name="statuses"),
    path("approve/", apis.approve_request, name="approve_request"),
    path("update_status/", apis.update_request_status, name="update_request_status"),
    path("complete/", apis.complete_request, name="complete_request"),
    path("emailproviders/", apis.get_email_providers, name="email_providers"),
    path("emailstats/", apis.get_request_stats, name="email_stats")
]