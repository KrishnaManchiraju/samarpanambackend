from django.contrib import admin
from samarpanam.apps.email_requests.models import (
    Audience,
    EmailType,
    Review,
    Status,
    EmailRequest,
)


class EmailRequestAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "email_subject",
        "email_type",
        "expected_delivery",
        "details",
        "created_by_name",
    )

    def created_by_name(self, obj):
        return obj.created_by.get_full_name()


admin.site.register(Audience)
admin.site.register(EmailType)
admin.site.register(Status)
admin.site.register(Review)
admin.site.register(EmailRequest, EmailRequestAdmin)