from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.utils.translation import gettext as _
from django.utils.timezone import now
import os
import logging

class EmailProvider(models.TextChoices):
    #name = value, label
    Mailchimp = "MAILCHIMP",  _("Mailchimp")
    Sendgrid = "SENDGRID", _("Sendgrid")
    __empty__ = ""

logger = logging.getLogger("samarpanam")

class Audience(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Status(models.Model):
    status = models.CharField(max_length=30, primary_key=True)
    step = models.IntegerField(unique=True)
    style_options = (
        ("badge-default", "Default"),
        ("badge-primary", "Primary"),
        ("badge-info", "Info"),
        ("badge-success", "Success"),
        ("badge-warning", "Warning"),
        ("badge-danger", "Danger"),
    )
    style = models.CharField(
        max_length=30, choices=style_options, default="badge-default"
    )

    def __str__(self):
        return self.status


class EmailType(models.Model):
    email_type = models.CharField(max_length=30)

    def __str__(self):
        return self.email_type


class EmailRequest(models.Model):
    email_subject = models.CharField(max_length=100)
    from_address = models.EmailField(default="uk@ishafoundation.org")
    audience = models.ManyToManyField(Audience)
    expected_delivery = models.DateField(_("Delivery Date"))
    # reviewers = models.ManyToManyField(User)
    details = models.TextField(blank=True, null=True)
    template = models.FileField(upload_to="./EmailTemplates", null=True, blank=True)
    created_by = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="created_by_name",
        related_query_name="created_by",
    )
    created_at = models.DateTimeField(_("Created at"), default=now)
    #
    modified_by = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="modified_by_name",
        related_query_name="modified_by",
        blank=True,
        null=True,
    )
    modified_at = models.DateTimeField(_("Modified at"), blank=True, null=True)
    status = models.ForeignKey(Status, on_delete=models.PROTECT, blank=True, null=True)
    email_type = models.ForeignKey(
        EmailType, on_delete=models.PROTECT, blank=True, null=True
    )
    email_provider = models.CharField(max_length=100, choices=EmailProvider.choices, blank=True, null=True)
    email_provider_request_ref = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        req_str = "Created by: {} \nSubject: {}\nExpected Delivery: {}\nExtra details: {}".format(
            self.created_by.get_full_name(),
            self.email_subject,
            self.expected_delivery,
            self.details,
        )
        return req_str

@receiver(pre_save, sender=EmailRequest)
def auto_delete_template_on_change(sender, instance, **kwargs):
    """
    Deletes old email template file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_template = sender.objects.get(pk=instance.pk).template
    except sender.DoesNotExist:
        return False

    if instance.template and old_template and instance.template != old_template:
        if os.path.isfile(old_template.path):
            os.remove(old_template.path)
            logger.info("{} file deleted.".format(old_template.path))
            return True

    return False

class Review(models.Model):
    request = models.ForeignKey(
        EmailRequest,
        related_name="reviews",
        on_delete=models.PROTECT,
    )
    reviewer = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
    )
    approved = models.BooleanField(default=False)
    approval_time = models.DateField(blank=True, null=True)
