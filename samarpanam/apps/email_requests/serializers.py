from rest_framework import serializers
from .models import EmailRequest, Audience, EmailType, Review, Status, EmailProvider
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source="get_full_name", required=False)
    class Meta:
        model = User
        fields = ["id", "email", "first_name", "name"]


class AudienceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audience
        fields = "__all__"


class EmailTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailType
        fields = "__all__"


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = "__all__"


class ReviewSerializer(serializers.ModelSerializer):
    reviewer = UserSerializer()
    class Meta:
        model = Review
        fields = "__all__"


class EmailRequestGetSerializer(serializers.ModelSerializer):
    audience = AudienceSerializer(many=True)
    reviews = ReviewSerializer(many=True, read_only=True)
    expected_delivery = serializers.DateField(format="%d.%m.%Y")
    created_by = serializers.ReadOnlyField(source="created_by.get_full_name")
    created_at = serializers.DateTimeField(format="%d.%m.%Y")
    status = StatusSerializer()
    email_type = EmailTypeSerializer()
    email_provider_desc = serializers.CharField(source='get_email_provider_display')

    class Meta:
        model = EmailRequest
        fields = "__all__"
        extra_fields = ["reviews"]

class EmailRequestGetPartialSerializer(serializers.ModelSerializer):
    expected_delivery = serializers.DateField(format="%d.%m.%Y")
    created_by = serializers.ReadOnlyField(source="created_by.get_full_name")
    email_type = serializers.ReadOnlyField(source="email_type.email_type")
    status = StatusSerializer()

    class Meta:
        model = EmailRequest
        fields = [
            "id",
            "email_subject",
            "expected_delivery",
            "created_by",
            "status",
            "email_type",
        ]


class EmailRequestPostSerializer(serializers.ModelSerializer):
    expected_delivery = serializers.DateField(input_formats=["%d.%m.%Y", "iso-8601"])

    class Meta:
        model = EmailRequest
        fields = [
            "email_subject",
            "from_address",
            "expected_delivery",
            "details",
            "created_by",
            "template",
        ]


class EmailRequestUpdateSerializer(serializers.ModelSerializer):
    expected_delivery = serializers.DateField(input_formats=["%d.%m.%Y", "iso-8601"])

    class Meta:
        model = EmailRequest
        fields = [
            "email_subject",
            "from_address",
            "expected_delivery",
            "details",
            "modified_by",
            "email_type",
            "status",
            "template"
        ]

class EmailProviderChoicesSerializer(serializers.Serializer):
    email_provider = serializers.SerializerMethodField()
    email_provider_desc = serializers.SerializerMethodField()

    def get_email_provider(self, obj):
        return obj[0]     

    def get_email_provider_desc(self, obj):        
        return obj[1] 

class EmailRequestCompletionSerializer(serializers.ModelSerializer):    
    class Meta:
        model = EmailRequest
        fields = [
            "email_provider",
            "email_provider_request_ref",
        ]

class EmailRequestgetCompletionSerializer(serializers.ModelSerializer):
    status = StatusSerializer()
    email_provider_desc = serializers.CharField(source='get_email_provider_display')

    class Meta:
        model = EmailRequest
        fields = [
            "email_provider",
            "email_provider_request_ref",
            "status"
        ]

class MailchimpStatsSerializer(serializers.Serializer):
    successul_deliveries = serializers.SerializerMethodField() 
    unique_opens = serializers.SerializerMethodField()
    unique_clicks = serializers.SerializerMethodField()
    total_clicks = serializers.SerializerMethodField()
    abuse_reports = serializers.SerializerMethodField()
    unsubscribed = serializers.SerializerMethodField()
    sent_at = serializers.SerializerMethodField()   

    def get_successul_deliveries(self, obj):
        return obj['emails_sent']

    def get_unique_opens(self, obj):        
        return obj['opens']['unique_opens']

    def get_unique_clicks(self, obj):        
        return obj['clicks']['unique_clicks']

    def get_total_clicks(self, obj):        
        return obj['clicks']['clicks_total']

    def get_abuse_reports(self, obj):        
        return obj['abuse_reports']

    def get_unsubscribed(self, obj):        
        return obj['unsubscribed']

    def get_sent_at(self, obj):        
        return obj['send_time']
