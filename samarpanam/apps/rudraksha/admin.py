from import_export.admin import ImportExportModelAdmin
from django.contrib import admin
from .models import RudrakshaRecord


@admin.register(RudrakshaRecord)
class PersonAdmin(ImportExportModelAdmin):
    list_display = (
        "reg_id",
        "first_name",
        "last_name",
        "email",
        "country",
        "is_updated",
        "phase",
    )
    list_filter = (
        "is_updated",
        "phase",
        "country",
    )
    search_fields = (
        "reg_id",
        "email",
        "first_name",
    )
