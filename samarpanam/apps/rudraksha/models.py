from django.db import models
from django.utils.translation import gettext as _

# Create your models here.
class RudrakshaRecord(models.Model):
    reg_id = models.CharField(max_length=20)
    first_name = models.CharField(max_length=300)
    last_name = models.CharField(max_length=300)
    email = models.EmailField(max_length=200)
    phone_number = models.CharField(max_length=20, default="")
    date_updated = models.DateField(_("DateUpdated"), blank=True, null=True)

    #
    country = models.CharField(max_length=100, blank=True)
    street = models.CharField(max_length=500, blank=True)
    house_number = models.CharField(max_length=100, blank=True)
    city = models.CharField(max_length=300, blank=True)
    postcode = models.CharField(max_length=100, blank=True)
    #
    is_updated = models.BooleanField(default=False)
    phase = models.IntegerField()
