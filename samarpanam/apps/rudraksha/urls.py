from django.urls import path
from . import apis

urlpatterns = [
    path("get_record/", apis.get_single_record, name="get record"),
    path("update/", apis.update_record, name="update record"),
]