from django.apps import AppConfig


class RudrakshaConfig(AppConfig):
    name = 'rudraksha'
