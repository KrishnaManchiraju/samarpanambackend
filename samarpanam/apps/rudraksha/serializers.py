from rest_framework import serializers

from .models import RudrakshaRecord


class RudrakshaRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = RudrakshaRecord
        # fields = "__all__"
        exclude = ("id", "is_updated", "date_updated", "phase")
