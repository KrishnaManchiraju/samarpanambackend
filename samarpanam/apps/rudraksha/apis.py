import logging
from datetime import date

from rest_framework.decorators import api_view, permission_classes
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework import permissions, status
from rest_framework.serializers import Serializer

from .models import RudrakshaRecord
from .serializers import RudrakshaRecordSerializer

logger = logging.getLogger("rudraksha")


@api_view(["GET"])
@permission_classes([permissions.AllowAny])
def get_single_record(request: Request) -> Response:
    id = request.GET.get("id")
    email = request.GET.get("email")

    try:
        rd_obj = RudrakshaRecord.objects.get(reg_id=id)

        if rd_obj.is_updated:
            logger.info(f"User: {id} trying again")
            return Response(
                {"message": "Record already submitted"}, status=status.HTTP_423_LOCKED
            )

        if str(rd_obj.email).lower() != str(email.strip()).lower():
            logger.info(f"User: {id} trying with wrong email")
            return Response(
                {"message": "Invalid Request"}, status=status.HTTP_404_NOT_FOUND
            )

        logger.info(f"User: {id} trying to fetch existing data")
        serializer = RudrakshaRecordSerializer(rd_obj)

        return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
    except RudrakshaRecord.DoesNotExist:
        logger.info(f"Invalid lookup for id: {id} attempted")

        return Response(
            {"message": "Record not found"}, status=status.HTTP_404_NOT_FOUND
        )


@api_view(["PUT"])
@permission_classes([permissions.AllowAny])
def update_record(request: Request) -> Response:
    id = request.data.get("reg_id")
    print(id)
    try:
        rd_obj = RudrakshaRecord.objects.get(reg_id=id)
        serializer = RudrakshaRecordSerializer(rd_obj, data=request.data)
        if serializer.is_valid():
            rd_obj.is_updated = True
            rd_obj.date_updated = date.today()
            serializer.save()
            logger.info(f"Data for id: {id} successfully updated")
            return Response({"message": "Success"}, status=status.HTTP_200_OK)
        else:
            return Response(
                {"message": "Invalid Data"}, status=status.HTTP_400_BAD_REQUEST
            )

    except RudrakshaRecord.DoesNotExist:
        logger.info(f"Invalid update for id: {id} attempted")

        return Response(
            {"message": "Record not found"}, status=status.HTTP_404_NOT_FOUND
        )
