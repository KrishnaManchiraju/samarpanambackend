from django.urls import path
from rest_framework_simplejwt import views as jwt_views

from . import apis

urlpatterns = [
    path("token/", apis.ObtainTokenPairWithUserDetailsView.as_view(), name="token_obtain_pair"),
    path("token/refresh/", jwt_views.TokenRefreshView.as_view(), name="token_refresh"),
    path(
        "signin_with_google/",
        apis.sign_in_or_register_user_with_google,
        name="google_login",
    ),
    path("register/", apis.register_old_school, name="register"),
    path("activate/", apis.activate_user, name="activate"),
    path("send_mail/", apis.send_my_email, name="send_mail"),
]