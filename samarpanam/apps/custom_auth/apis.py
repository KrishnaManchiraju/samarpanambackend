from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User

from rest_framework.decorators import api_view, permission_classes
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status, permissions
from rest_framework.utils import json
from rest_framework_simplejwt.views import TokenObtainPairView

from samarpanam.emails import email_service

import requests

from .serializers import CustomTokenObtainPairSerializer


@api_view(["POST"])
@permission_classes([permissions.AllowAny])
def sign_in_or_register_user_with_google(request: Request) -> Response:
    """
    Sign in or Register a user with google oauth api.
    Re-validates the token and gets the user profile

    Inputs:
    Expects an access token in the request.

    Returns:
    web tokens if registered/logged in successfully

    """
    payload = {"access_token": request.data.get("access_token")}

    # validate the token
    r = requests.get("https://www.googleapis.com/oauth2/v2/userinfo", params=payload)
    data = json.loads(r.text)

    if "error" in data:
        content = {"message": "Invalid or Expired token"}
        return Response(data=content, status=status.HTTP_400_BAD_REQUEST)

    user, created = User.objects.get_or_create(
        username=data["email"],
        email=data["email"],
        defaults={
            "password": make_password(BaseUserManager().make_random_password()),
            "first_name": data["given_name"],
            "last_name": data["family_name"],
            # "is_active": False
        },
    )

    ret_status = status.HTTP_201_CREATED if created else status.HTTP_202_ACCEPTED

    # generate token
    token = RefreshToken.for_user(user)
    token["user"] = user.first_name
    token["is_admin"] = user.is_staff

    # build response
    response = {}
    response["access"] = str(token.access_token)
    response["refresh"] = str(token)

    return Response(data=response, status=ret_status)


@api_view(["GET"])
def send_my_email(request: Request) -> Response:
    email_service.send_verification_email(
        request.data["name"], request.data["email"], "some temp link"
    )

    return Response(status=status.HTTP_200_OK)


@api_view(["POST"])
@permission_classes([permissions.AllowAny])
def register_old_school(request: Request) -> Response:
    """
    To register a user in an old school way. i.e. by filling out the registration form

    Inputs:
        Expects the following in the request:
        1. first_name
        2. last_name
        3. email
        4. password

    """
    user, created = User.objects.get_or_create(
        username=request.data["email"],
        email=request.data["email"],
        defaults={
            "password": request.data["password"],
            "first_name": request.data["first_name"],
            "last_name": request.data["last_name"],
            "is_active": False,
        },
    )

    if created:
        content = {"message": "Successfully registered"}
        ret_status = status.HTTP_201_CREATED
        email_service.send_verification_email(
            user.first_name, user.email, user.profile.secret_key
        )

    else:
        content = {"message": "User alreay exists"}
        ret_status = status.HTTP_400_BAD_REQUEST

    return Response(data=content, status=ret_status)


@api_view(["PUT"])
@permission_classes([permissions.AllowAny])
def activate_user(request: Request) -> Response:
    """
    If user registered in the old school way, one has to verify their email.
    Once verfied we activate the user here

    Inputs:
    1. email
    2. verification code

    Returns:
    Web tokens if successful
    """
    try:
        user = User.objects.get(username=request.data["email"])

        if request.data["code"] == user.profile.secret_key and not user.is_active:
            user.is_active = True
            user.save()
            # generate token
            token = RefreshToken.for_user(user)
            response = {}
            response["username"] = user.username
            response["first_name"] = user.first_name
            response["access"] = str(token.access_token)
            response["refresh"] = str(token)
            ret_status = status.HTTP_202_ACCEPTED
        else:
            response = {"message": "Incorrect Code"}
            ret_status = status.HTTP_401_UNAUTHORIZED

        return Response(data=response, status=ret_status)

    except User.DoesNotExist:
        content = {"message": "User does not exist"}
        ret_status = status.HTTP_400_BAD_REQUEST

        return Response(data=content, status=ret_status)


class ObtainTokenPairWithUserDetailsView(TokenObtainPairView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = CustomTokenObtainPairSerializer