import logging
from typing import List

from django.core import mail
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.utils.html import strip_tags

logger = logging.getLogger("samarpanam")
from_email = "Isha EU Emails <eu.emails@ishafoundation.org>"


def send_verification_email(name: str, email: str, secret_code: str):
    try:
        url = "https://samarpanam.ishaeurope.org/verify?email={}&code={}".format(email, secret_code)

        subject = "Confirm your email address"
        html_message = render_to_string(
            "verify_email.html", {"name": name, "verification_link": url}
        )
        plain_message = strip_tags(html_message)
        from_email = "Isha EU Emails <eu.emails@ishafoundation.org>"
        to = "{} <{}>".format(name, email)

        mail.send_mail(
            subject, plain_message, from_email, [to], html_message=html_message
        )
    except Exception as e:
        logger.error("send_verification_email: " + str(e))


def send_create_notification(req):
    try:
        subject = "New Email Request"
        plain_message = (
            "Namaskaram,\n\n"
            "A new email request has been made on Samarpanam.\n\n"
            "Request: https://samarpanam.ishaeurope.org/detail/{}"
            "Here are the details:\n\n"
            "{}\n\nPranam"
        ).format(req.id, str(req))

        requester = req.created_by.email
        cc_lst = []
        for rev in req.reviews.all():
            cc_lst.append(rev.reviewer.email)
        if requester not in cc_lst:
            cc_lst.append(requester)

        admins = []
        for adm in User.objects.filter(is_staff=True):
            admins.append(adm.email)

        # mail.send_mail(subject, plain_message, from_email, [to])
        email = EmailMessage(
            subject=subject,
            body=plain_message,
            from_email=from_email,
            to=admins,
            cc=cc_lst,
        )
        email.send()

    except Exception as e:
        logger.error("send_create_notification: " + str(e))


def send_work_in_progress(
    id: int, subject_line: str, reviewers: List[str], admins: List[str]
):
    plain_message = (
        "Namaskaram Reviewers,\n\n"
        "Our team started working on your request. Please expect a review soon.\n\n"
        "Request: https://samarpanam.ishaeurope.org/detail/{}"
        "\nYou will recieve another email when the review is open.\n\n"
        "Pranam,\n"
        "EU Emailing Team (Samarpanam Bot)"
    ).format(id)

    email = EmailMessage(
        subject=subject_line,
        body=plain_message,
        from_email=from_email,
        to=reviewers,
        cc=admins,
    )
    email.send()


def send_out_for_review(
    id: int, subject_line: str, reviewers: List[str], admins: List[str]
):
    plain_message = (
        "Namaskaram Reviewers,\n\n"
        "A test mail has been sent. for the below request\n\n"
        "Request: https://samarpanam.ishaeurope.org/detail/{}"
        "\nKindly review and take appropriate action in the link above."
        "\n\nPranam,\n"
        "EU Emailing Team (Samarpanam Bot)"
    ).format(id)

    email = EmailMessage(
        subject=subject_line,
        body=plain_message,
        from_email=from_email,
        to=reviewers,
        cc=admins,
    )
    email.send()


def send_all_approved(
    id: int, subject_line: str, reviewers: List[str], admins: List[str]
):
    plain_message = (
        "Namaskaram Team,\n\n"
        "The current request is in all approved state.\n\n"
        "Request: https://samarpanam.ishaeurope.org/detail/{}"
        "\n\nPranam,\n"
        "EU Emailing Team (Samarpanam Bot)"
    ).format(id)

    email = EmailMessage(
        subject=subject_line,
        body=plain_message,
        from_email=from_email,
        to=admins,
        cc=reviewers,
    )
    email.send()


def send_email_sent(
    id: int, subject_line: str, reviewers: List[str], admins: List[str]
):
    plain_message = (
        "Namaskaram,\n\n"
        "This email has been sent as per the below request.\n\n"
        "Request: https://samarpanam.ishaeurope.org/detail/{}"
        "\n\nPranam,\n"
        "EU Emailing Team (Samarpanam Bot)"
    ).format(id)

    email = EmailMessage(
        subject=subject_line,
        body=plain_message,
        from_email=from_email,
        to=reviewers,
        cc=admins,
    )
    email.send()


def send_status_update(req):
    current_step = req.status.step
    requester = req.created_by.email

    reviewers = []
    for rev in req.reviews.all():
        reviewers.append(rev.reviewer.email)
    if requester not in reviewers:
        reviewers.append(requester)

    admins = []
    for adm in User.objects.filter(is_staff=True):
        admins.append(adm.email)

    subject_line = f"Samarpanam updates: {req.email_subject}"

    if current_step == 3:  # in progress
        send_work_in_progress(req.id, subject_line, reviewers, admins)
    elif current_step == 4:  # sent out for review
        send_out_for_review(req.id, subject_line, reviewers, admins)
    elif current_step == 5:  # all approved
        send_all_approved(req.id, subject_line, reviewers, admins)
    elif current_step == 6:  # email sent
        send_email_sent(req.id, subject_line, reviewers, admins)
